Description
-----------

After Protector Myssil died, some say by the daggers of a shadowy backstabber,
while others bespoke of a crazed mage drawing fiendish black magicks,
the fanatic Ziguranth order crumbled into chaos, and its numbers dwindled
into insignificance.


Gameplay
--------

Completely removes Ziguranth patrols after Myssil dies.

-----

`addon <http://te4.org/games/addons/tome/ziguranth-genocide>`_ | `forum <http://forums.te4.org/viewtopic.php?f=50&t=41983&page=1>`_ | `steam <https://steamcommunity.com/sharedfiles/filedetails/?id=2641688399>`
